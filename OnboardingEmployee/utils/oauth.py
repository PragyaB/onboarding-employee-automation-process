from __future__ import print_function

import json, requests, logging
from OnboardingEmployee.settings import GITLAB_TOKEN, GITLAB_URL, YOUTRACK_TOKEN, YOUTRACK_URL, SENDGRID_API_KEY

from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

SCOPES = ["https://www.googleapis.com/auth/admin.directory.user"]


def oauth_token(employee_instance):
    """
    Shows basic usage of the Admin SDK Directory API.
    """
    # import pdb; pdb.set_trace()
    try:
        creds = None
        if employee_instance.user_id.oauth_token:
            info = json.loads(employee_instance.user_id.oauth_token)
            creds = Credentials.from_authorized_user_info(info, SCOPES)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    "credentials.json", SCOPES
                )
                creds = flow.run_local_server(port=0)
            # Save the credentials for the next run
            employee_instance.user_id.oauth_token = creds.to_json()
            employee_instance.user_id.save()
        return creds
    except Exception as e:
        logging.error(e)


def create_gmail(first_name, last_name, email, employee_instance):
    try:
        creds = oauth_token(employee_instance)
        service = build("admin", "directory_v1", credentials=creds)

        # Call the Admin SDK Directory API
        userInfo = {
            "name": {"givenName": first_name, "familyName": last_name},
            "kind": "user",
            "primaryEmail": f"{first_name.lower()}@linarc.io",
            "secondaryEmail": email,
            "isDelegatedAdmin": False,
            "suspended": False,
            "isAdmin": False,
            "agreedToTerms": True,
            "password": "default-password",
            "changePasswordAtNextLogin": True,
        }
        service.users().insert(body=userInfo).execute()

        message = Mail(
                    from_email='info@linarc.io',
                    to_emails=email,
                    subject='Welcome to Linarc',
                    html_content='''
                    <p>
                    <strong>Hello {name}, </strong><br>
                    <br>
                    Welcome to Linarc Inc. Please login to your Linarc Mail using the following credentials<br>
                    <br>
                    Email: {email}<br>
                    Password: default_password<br>
                    <br>
                    Thank You.<br>
                    <br>
                    Regards,<br>
                    Team Linarc
                    </p>
                    '''.format(name=employee_instance.first_name, email=f"{first_name.lower()}@linarc.io"))
                
        sg = SendGridAPIClient(SENDGRID_API_KEY)
        sg.send(message)
        return f"{first_name.lower()}@linarc.io"
    except Exception as e:
        logging.error(e)

def invite_drive(employee_instance):
    try:
        creds = oauth_token(employee_instance)
        service = build("admin", "directory_v1", credentials=creds)
        return True
    except Exception as e:
        logging.error(e)

# Function to invite a new user to Gitlab

def invite_gitlab(employee_instance):

    try:
        url = GITLAB_URL
        url += "?email={email}&access_level=30".format(email = f"{employee_instance.first_name.lower()}@linarc.io")
        headers = {
            'PRIVATE-TOKEN': GITLAB_TOKEN
            }
        
        response = requests.request("POST", url, headers=headers)
        return True
    except Exception as e:
        logging.error(e)

# Function to invite a new user to GitLab

def invite_youtrack(employee_instance):

    try:
        url = YOUTRACK_URL
        url += "?failOnPermissionReduce=true&email={email}&fields=id".format(email = f"{employee_instance.first_name.lower()}@linarc.io")
        headers = {
            'Authorization': YOUTRACK_TOKEN
            }
        
        response = requests.request("POST", url, headers=headers)
        return True
    except Exception as e:
        logging.error(e)


if __name__ == "__main__":
    oauth_token()
