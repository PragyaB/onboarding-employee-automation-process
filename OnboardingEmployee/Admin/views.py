from django.shortcuts import render
from django.contrib.auth.mixins import (
    LoginRequiredMixin,
)
from django.views.generic import TemplateView
from requests import request

from Admin.utils import create_user_accounts
from .models import *
from .forms import *
from django.contrib.auth.views import LoginView
from django.views.generic import (
    CreateView,
    DetailView,
)
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from .forms import SignUpForm
from django.urls import reverse_lazy
from django.contrib import messages

# Create your views here.


class CompanySetupView(LoginRequiredMixin, TemplateView):
    login_url = "/login/"
    redirect_field_name = "login"

    def get(self, request):
        return render(request, "setup.html")


class Platformselectionview(LoginRequiredMixin, TemplateView):
    login_url = "/login/"
    redirect_field_name = "login"

    def get(self, request):
        user_id = request.user.admin.user_id
        all_platforms = company_platforms.objects.filter(user_id=user_id)
        plt_ids = all_platforms.values_list("plt_id", flat=True)
        system_platforms = system_platform.objects.exclude(plt_id__in=plt_ids)
        context = {
            "all_platforms": all_platforms,
            "system_platforms": system_platforms,
            "show_list": True,
        }
        return render(request, "platform_setup.html", context)

    def post(self, request):
        user_id = request.user.admin
        data = request.POST
        selected_platforms = {x: y for x, y in data.items() if y == "on"}
        for platform in selected_platforms:
            p = company_platforms(
                plt_id=system_platform.objects.get(plt_id=platform), user_id=user_id
            )
            p.save()

        return redirect("platform")


class Roleselectionview(LoginRequiredMixin, TemplateView):
    login_url = "/login/"
    redirect_field_name = "login"

    def get(self, request):
        user_id = request.user.admin.user_id
        all_roles = company_roles.objects.filter(user_id=user_id)
        form = AddRole()
        context = {"all_roles": all_roles, "form": form}
        return render(request, "role_setup.html", context)

    def post(self, request):
        form = AddRole(request.POST)
        admin_user = request.user.admin
        if form.is_valid():
            cmp_role = form.save()
            cmp_role.user_id = admin_user
            cmp_role.save()
            return redirect("Roles")
        else:
            return redirect("Roles")


class PlatforandRolesmselectionview(LoginRequiredMixin, TemplateView):
    login_url = "/login/"
    redirect_field_name = "login"

    def get(self, request, role_id):
        role_platforms = role_platform.objects.filter(
            role_id=role_id, user_id=request.user.admin.user_id
        )
        cpl_ids = role_platforms.values_list("cpl_id", flat=True)
        all_platforms = company_platforms.objects.filter(
            user_id=request.user.admin.user_id
        ).exclude(cpl_id__in=cpl_ids)
        role = company_roles.objects.get(
            role_id=role_id, user_id=request.user.admin.user_id
        )
        context = {
            "all_platforms": all_platforms,
            "role_platforms": role_platforms,
            "role": role,
        }
        return render(request, "link_platform_role.html", context)

    def post(self, request, role_id):
        # import pdb; pdb.set_trace()
        user_id = request.user.admin
        data = request.POST
        role = company_roles.objects.get(role_id=role_id)
        selected_platforms = {x: y for x, y in data.items() if y == "on"}
        for platform in selected_platforms:
            p = role_platform(
                cpl_id=company_platforms.objects.get(plt_id=platform, user_id=user_id),
                role_id=role,
                user_id=user_id,
            )
            p.save()

        return redirect("plfroles", role_id=role_id)


def index(request):
    return render(request, "authpage.html", {"title": "index"})


def signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            print(form.cleaned_data)
            user.admin.user_phone = form.cleaned_data.get("user_phone")
            user.admin.user_company_name = form.cleaned_data.get("user_company_name")
            user.admin.save()
            return redirect("login")
        else:
            message = form.errors
            print(message)
    else:
        form = SignUpForm()
    return render(request, "Admin/signup.html", {"form": form})


def Login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            form = login(request, user)
            messages.success(request, f" welcome {username} !!")
            return redirect("employees")
        else:
            messages.info(request, f"account done not exit plz sign in")
            print("Enter the correct username & password")
    form = AuthenticationForm()
    return render(request, "Admin/login.html", {"form": form, "title": "log in"})


class OnboardView(LoginRequiredMixin, CreateView):
    login_url = "/login/"
    redirect_field_name = "login"
    form_class = UserAddForm
    template_name = "userdetails.html"

    def get_form_kwargs(self):
        kwargs = super(OnboardView, self).get_form_kwargs()
        kwargs['user_id'] = self.request.user.admin.user_id # pass the 'user' in kwargs
        return kwargs

    def form_valid(self, form):
        # import pdb

        # pdb.set_trace()
        role = form.cleaned_data["role"]
        if not role_platform.objects.filter(
            role_id=role.role_id, user_id=self.request.user.admin.user_id
        ).exists():
            return render(
                self.request,
                "userdetails.html",
                {"role_error": True, "role": role.role_name},
            )
        self.object = form.save()
        self.object.user_id = self.request.user.admin
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        context_data = self.get_context_data()
        emp_id = context_data["employee"].emp_id
        return reverse_lazy("userdetail", kwargs={"pk": emp_id})


class CreateAccounts(LoginRequiredMixin, TemplateView):
    login_url = "/login/"
    redirect_field_name = "login"

    def get(self, request, emp_id):
        employee = Employee.objects.get(emp_id=emp_id)
        employee.send_ro_process = True
        employee.account_creation = Employee.processing.PROCESSING
        employee.save()
        create_user_accounts(employee)
        return redirect("userdetail", pk=emp_id)


class EmployeeList(LoginRequiredMixin, TemplateView):
    login_url = "/login/"
    redirect_field_name = "login"

    def get(self, request):
        user_id = request.user.admin.user_id
        employees = Employee.objects.filter(user_id=user_id)
        context = {"employees": employees}
        return render(request, "userslist.html", context)


class EmployeeDetail(LoginRequiredMixin, DetailView):
    login_url = "/login/"
    redirect_field_name = "login"
    model = Employee
    template_name = "userdetail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
