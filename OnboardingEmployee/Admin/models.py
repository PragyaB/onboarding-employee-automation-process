from django.db import models
from django.db.models.deletion import CASCADE
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Admin(models.Model):
    class user_state(models.TextChoices):
        AdminSetup = "AS", ("AdminSetup")
        PlatformSetup = "PL", ("Platform")
        RoleSetup = "RL", ("Role")
        LinkedRoleWithPlatform = "LRP", ("LinkedRP")

    user_id = models.AutoField(primary_key=True)
    user_auth = models.OneToOneField(User, on_delete=models.CASCADE)
    user_phone = models.CharField(unique=True, max_length=15, null=True, db_index=True)
    user_company_name = models.CharField(
        max_length=10,
    )
    user_createdon = models.DateTimeField(auto_now=True)
    user_status = models.CharField(
        max_length=3,
        choices=user_state.choices,
        default=user_state.AdminSetup,
        null=True,
    )
    oauth_token = models.JSONField(default={})

    class Meta:
        db_table = "admin"

    @receiver(post_save, sender=User)
    def update_user_profile(sender, instance, created, **kwargs):
        if created:
            Admin.objects.create(user_auth=instance)
            instance.admin.save()


class system_platform(models.Model):

    plt_id = models.AutoField(primary_key=True)
    platform_name = models.CharField(max_length=15)

    class Meta:
        db_table = "system_platform"


class company_platforms(models.Model):

    cpl_id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(Admin, on_delete=CASCADE, db_column="user_id")
    plt_id = models.ForeignKey(system_platform, on_delete=CASCADE, db_column="plt_id")

    class Meta:
        db_table = "company_platform"


class company_roles(models.Model):

    role_id = models.AutoField(primary_key=True)
    role_name = models.CharField(max_length=15)
    user_id = models.ForeignKey(Admin, on_delete=CASCADE, db_column="user", null=True, blank=True)

    class Meta:
        db_table = "company_roles"
    
    def __str__(self):
        return self.role_name


class role_platform(models.Model):

    rpl_id = models.AutoField(primary_key=True)
    role_id = models.ForeignKey(company_roles, on_delete=CASCADE, db_column="role_id")
    cpl_id = models.ForeignKey(company_platforms, on_delete=CASCADE, db_column="plt_id")
    user_id = models.ForeignKey(Admin, on_delete=CASCADE, db_column="user", null=True, blank=True)

    class Meta:
        db_table = "role_platform"


class Employee(models.Model):
    class processing(models.TextChoices):
        PENDING = "PENDING", ("PENDING")
        PROCESSING = "PROCESSING", ("PROCESSING")
        DONE = "DONE", ("DONE")
        FAILED = "FAILED", ("FAILED")

    emp_id = models.AutoField(primary_key=True)
    first_name = models.CharField(
        max_length=30, name="first_name", null=True, blank=True
    )
    last_name = models.CharField(max_length=30, name="last_name", null=True, blank=True)
    email_account = models.CharField(
        unique=True,
        max_length=250,
        db_index=True,
        name="email_account",
        null=True,
        blank=True,
    )
    phone = models.CharField(unique=True, max_length=10, db_index=True, null=True, blank=True)
    role = models.ForeignKey(
        company_roles, on_delete=models.CASCADE, db_column="role", null=True, blank=True
    )
    company_email = models.CharField(
        unique=True,
        max_length=250,
        db_index=True,
        null=True,
        blank=True,
        name="company_email",
    )
    emp_createdon = models.DateTimeField(auto_now=True)
    user_id = models.ForeignKey(Admin, on_delete=CASCADE, db_column="user", null=True, blank=True)
    account_creation = models.CharField(
        max_length=15,
        choices=processing.choices,
        default=processing.PENDING,
        null=True,
    )
    google_account = models.BooleanField(
    )
    google_drive = models.BooleanField(
    )
    youtrack_account = models.BooleanField(
    )
    gitlab_account = models.BooleanField(
    )
    send_ro_process = models.BooleanField(default=False)

    class Meta:
        db_table = "employee"

