from django import forms
from .models import *


# class PlatformForm(forms.ModelForm):
#     # cpl_id = forms.CharField(primary_key=True, max_length=15)
#     # user_id = forms.CharField(max_length=50)
#     # plt_id = forms.CharField(max_length=20)
#     # platform_name = forms.CharField(max_length=50)
#     # class Meta:
#     #     model = company_platforms
#     #     fields = ('user_id','plt_id')
#     platform_name = forms.CharField(label='checks', max_length=100)
#     class Meta:
#         model = company_platforms
#         fields = ('platform_name')
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    user_phone = forms.CharField(max_length=20)
    user_company_name = forms.CharField(max_length=20)

    class Meta:
        model = User
        fields = (
            "username",
            "first_name",
            "last_name",
            "email",
            "user_phone",
            "user_company_name",
            "password1",
            "password2",
        )


class UserAddForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        user_id = kwargs.pop("user_id")
        super().__init__(*args, **kwargs)
        self.fields["role"].queryset = company_roles.objects.filter(
            user_id=user_id
        )

    class Meta:
        model = Employee
        exclude = ("company_email", "user_id", "account_creation", "send_ro_process")


class AddRole(forms.ModelForm):
    class Meta:
        model = company_roles
        exclude = ("user_id",)
