import logging
from Admin.models import role_platform
from utils.oauth import create_gmail, invite_drive, invite_gitlab, invite_youtrack

# Function to Create User Accounts based on the Role in Employee Instance

def create_user_accounts(employee_instance):
    try:
        first_name = employee_instance.first_name
        last_name = employee_instance.last_name
        email = employee_instance.email_account
        # Fecthing Accounts to be created for the Employee
        accounts_to_be_created = role_platform.objects.filter(
            role_id=employee_instance.role
        )
        cmp_email = None
        # Creating each account for the selected Role
        for account in accounts_to_be_created:
            match account.cpl_id.plt_id.platform_name:
                case "Google Account":
                    if employee_instance.google_account:
                        cmp_email = create_gmail(first_name, last_name, email, employee_instance)
                        employee_instance.account_creation = "DONE"
                case "Google Drive":
                    if employee_instance.google_drive:
                        invite_drive(employee_instance)
                        employee_instance.account_creation = "DONE"
                case "Gitlab":
                    if employee_instance.gitlab_account:
                        invite_gitlab(employee_instance)
                        employee_instance.account_creation = "DONE"
                case "Youtrack":
                    if employee_instance.youtrack_account:
                        invite_youtrack(employee_instance)
                        employee_instance.account_creation = "DONE"
                    else:
                        employee_instance.youtrack_account = "Not Created"
                case _:
                    employee_instance.account_creation = "PENDING"
        # Save Company Email ID to Employee Instance
        employee_instance.company_email = cmp_email
        employee_instance.save()
    except Exception as e:
        logging.error(e)
        employee_instance.account_creation = "FAILED"
        employee_instance.save()
