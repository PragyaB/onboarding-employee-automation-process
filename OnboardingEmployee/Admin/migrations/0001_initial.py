# Generated by Django 3.2.6 on 2022-05-28 12:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('user_id', models.AutoField(primary_key=True, serialize=False)),
                ('user_phone', models.CharField(db_index=True, max_length=15, null=True, unique=True)),
                ('user_company_name', models.CharField(max_length=10)),
                ('user_createdon', models.DateTimeField(auto_now=True)),
                ('user_status', models.CharField(choices=[('AS', 'AdminSetup'), ('PL', 'Platform'), ('RL', 'Role'), ('LRP', 'LinkedRP')], default='AS', max_length=3, null=True)),
                ('user_auth', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'admin',
            },
        ),
        migrations.CreateModel(
            name='company_platforms',
            fields=[
                ('cpl_id', models.AutoField(primary_key=True, serialize=False)),
            ],
            options={
                'db_table': 'company_platform',
            },
        ),
        migrations.CreateModel(
            name='company_roles',
            fields=[
                ('role_id', models.AutoField(primary_key=True, serialize=False)),
                ('role_name', models.CharField(max_length=15)),
                ('user_id', models.ForeignKey(db_column='user_id', on_delete=django.db.models.deletion.CASCADE, to='Admin.admin')),
            ],
            options={
                'db_table': 'company_roles',
            },
        ),
        migrations.CreateModel(
            name='Employee',
            fields=[
                ('emp_id', models.AutoField(primary_key=True, serialize=False)),
                ('emp_first_name', models.CharField(max_length=30)),
                ('emp_last_name', models.CharField(max_length=30)),
                ('emp_address', models.CharField(max_length=300)),
                ('email', models.CharField(db_index=True, max_length=250, unique=True)),
                ('cmp_email', models.CharField(db_index=True, max_length=250, unique=True)),
                ('emp_phone', models.CharField(db_index=True, max_length=10, unique=True)),
                ('emp_createdon', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'Employee',
            },
        ),
        migrations.CreateModel(
            name='system_platform',
            fields=[
                ('plt_id', models.AutoField(primary_key=True, serialize=False)),
                ('platform_name', models.CharField(max_length=15)),
            ],
            options={
                'db_table': 'system_platform',
            },
        ),
        migrations.CreateModel(
            name='role_platform',
            fields=[
                ('rpl_id', models.AutoField(max_length=15, primary_key=True, serialize=False)),
                ('cpl_id', models.ForeignKey(db_column='plt_id', on_delete=django.db.models.deletion.CASCADE, to='Admin.company_platforms')),
                ('role_id', models.ForeignKey(db_column='user_id', on_delete=django.db.models.deletion.CASCADE, to='Admin.company_roles')),
            ],
            options={
                'db_table': 'role_platform',
            },
        ),
        migrations.AddField(
            model_name='company_platforms',
            name='plt_id',
            field=models.ForeignKey(db_column='plt_id', on_delete=django.db.models.deletion.CASCADE, to='Admin.system_platform'),
        ),
        migrations.AddField(
            model_name='company_platforms',
            name='user_id',
            field=models.ForeignKey(db_column='user_id', on_delete=django.db.models.deletion.CASCADE, to='Admin.admin'),
        ),
    ]
