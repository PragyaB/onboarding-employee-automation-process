# Generated by Django 3.2.6 on 2022-05-28 21:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Admin', '0011_alter_employee_table'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='send_ro_process',
            field=models.BooleanField(default=False),
        ),
    ]
