# Generated by Django 3.2.6 on 2022-05-28 14:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Admin', '0002_alter_role_platform_rpl_id'),
    ]

    operations = [
        migrations.AlterField(
            model_name='employee',
            name='cmp_email',
            field=models.CharField(blank=True, db_index=True, max_length=250, null=True, unique=True),
        ),
    ]
