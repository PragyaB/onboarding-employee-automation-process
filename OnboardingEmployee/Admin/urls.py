from django.urls import path
from django.conf import settings
from . import views

# from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path("", views.CompanySetupView.as_view(), name="home"),
    path("onboard/", views.OnboardView.as_view(), name="onboard"),
    path("platform-setup/", views.Platformselectionview.as_view(), name="platform"),
    path("role-setup/", views.Roleselectionview.as_view(), name="Roles"),
    path(
        "add-role-platform/<role_id>/",
        views.PlatforandRolesmselectionview.as_view(),
        name="plfroles",
    ),
    path("employees/", views.EmployeeList.as_view(), name="employees"),
    path("create-accounts/<emp_id>/", views.CreateAccounts.as_view(), name="create_accounts"),
    path("employee/<pk>/", views.EmployeeDetail.as_view(), name="userdetail"),
]

urlpatterns += staticfiles_urlpatterns()