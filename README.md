# Onboarding Employee Automation Process

### Automating the onboarding process for employees to give access to different platforms

*Follow these steps to access the project:*
1. Enter this address [ASAP Onboarding](http://13.234.31.159) in the browser,will land up home page
2. Do sign up if u have already signed up or just log in, will landup to page for company setup
3. Do platform setup
4. Add roles(designation) for the company
5. Enter the details for employee to onboard
6. To create account of comoany domain click on create account button
7. Employee list shows account creation status
8. Logout !!

### Steps to run code in local
---
- clone the code with https/ssh
- create virual environment in cloned folder -> python3 -m venv virtualenvironment
- install packages from requirements.txt -> pip install -r requirements.txt
- cd OnboardingEmployee
- run python manage.py migrate
- run python manage.py runserver

The reference link to highlevel diagram added here -> [HIGH LEVEL DIAGRAM](https://lucid.app/lucidchart/bb204b78-a209-46ee-ab42-31873347af21/edit?invitationId=inv_052ac3de-c7da-4eb4-9080-d7ef697f05b1#)

Gitlab Repsitory - > [ASAP GITLAB REPO](https://gitlab.com/PragyaB/onboarding-employee-automation-process)

---
